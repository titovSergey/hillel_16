<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 16.02.2020
 * Time: 11:33
 */

namespace App\Model;


use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function user()
    {
        return $this->belongsTo(\App\Model\User::class);
    }
}