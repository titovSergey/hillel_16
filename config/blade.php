<?php

$engineResolver = new \Illuminate\View\Engines\EngineResolver();

$fileSystem = new \Illuminate\Filesystem\Filesystem();
$fileViewFinder = new \Illuminate\View\FileViewFinder(
    $fileSystem, ['../resources/views']
);

$compiler = new \Illuminate\View\Compilers\BladeCompiler($fileSystem, '../resources/cache');

$engineResolver->register('blade', function () use ($compiler) {
    return new \Illuminate\View\Engines\CompilerEngine($compiler);
});

$container = new \Illuminate\Container\Container();
$dispatcher = new \Illuminate\Events\Dispatcher($container);

$blade = new \Illuminate\View\Factory(
    $engineResolver,
    $fileViewFinder,
    $dispatcher
);

$blade->composer('layouts.layout', function ($view){
//    var_dump(\App\Model\Navigation::all());
//    exit;
   $view->with('nav', [
       [
           'href' => '/',
           'name' => 'home',
           'title' => 'Home',
       ],
       [
           'href' => '/blog',
           'name' => 'blog',
           'title' => 'Blog',
       ],
       [
           'href' => '/services',
           'name' => 'services',
           'title' => 'Services',
       ],
       [
           'href' => '/team',
           'name' => 'team',
           'title' => 'Team',
       ],
       [
           'href' => '/contact-us',
           'name' => 'contacts',
           'title' => 'Contacts',
       ]
   ]);
});


//$blade->composer('layouts.layout', function ($view){
//    $view->with('nav', \App\Model\Navigation::all());
//});

